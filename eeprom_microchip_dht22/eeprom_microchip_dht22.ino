#include <Wire.h>
#include "Barometer.h"
#include <Rtc_Pcf8563.h>
#include <dht.h>
dht DHT;
#define DHT22_PIN 2
struct packet {
 uint8_t yr;
 uint8_t mo;
 uint8_t da;
 uint8_t ho;
 uint8_t mi;
 uint8_t se;
 uint16_t temp;
 uint32_t pres;
 uint16_t pad1;
 uint16_t pad2;
}writePacket;//must be 16bytes
Rtc_Pcf8563 rtc;
Barometer myBarometer;
int timer1_counter;
bool flag = 0;
unsigned int counter = 0;
unsigned int counter2 = 0;
byte highByte1;
byte lowByte1;
float temperature = 0;
long pressure = 0;
unsigned int pressure2 = 0;
bool mode = 0;
void setup() {
  noInterrupts();  
  TCCR1A = 0;
  TCCR1B = 0;
  timer1_counter = 3036; 
  TCNT1 = timer1_counter;   // preload timer
  TCCR1B |= (1 << CS12);    // 256 prescaler 
  TIMSK1 |= (1 << TOIE1);   // enable timer overflow interrupt
  interrupts(); 
  pinMode(10, OUTPUT);
  digitalWrite(10, HIGH);
  pinMode(11, OUTPUT);
  pinMode(13, OUTPUT);
  digitalWrite(11, HIGH);//DHT22
  pinMode(9, OUTPUT);//DHT22
  digitalWrite(9, HIGH);
  Serial.begin(9600);
  Wire.begin();
  rtc.initClock();
  //set a time to start with.
  //day, weekday, month, century(1=1900, 0=2000), year(0-99)
  rtc.setDate(1, 1, 1, 0, 14);
  //hr, min, sec
  rtc.setTime(1, 1, 1);
  pinMode(6, INPUT);
  myBarometer.init();
  delay(100);
}
void serialEvent(){
  byte data = Serial.read();

      //Serial.println(data);
  if(data == 0xFF){//realtime mode
    mode = 0;
    counter = 0;
  }
  if(data == 0x00){//download mode
    mode = 1;
  }
}
void loop() {
if(!mode){
      if(flag){ 
      if(counter > (32768-16)){
Serial.println("EEPROM FULL");
digitalWrite(13, HIGH);
}
else{      
            readSensors();
            eeprom_i2c_write(0x50,counter);            
            flag = !flag;
            //Serial.println("writing...");
            }
          counter2 = 0;

        }  
  }
  else{
  eeprom_i2c_read(0x50, counter2);
  delay(1);
  }
}
ISR(TIMER1_OVF_vect)        // interrupt service routine 
{
  TCNT1 = timer1_counter;   // preload timer
  flag = true;
}

void eeprom_i2c_write(byte address ,unsigned int from_addr) {
  
  Wire.beginTransmission(address);
  Wire.write((int)(from_addr >> 8));
  Wire.write((int)(from_addr & 0xFF));
  
  for(int i=0; i<sizeof(writePacket); i++)
  {
    Wire.write(((char*)&writePacket)[i]);
  }
  Wire.endTransmission();
  delay(5);
  eeprom_i2c_read(0x50, counter);
  counter = counter+sizeof(writePacket);
}
void eeprom_i2c_read(int address, int from_addr) {
  Wire.beginTransmission(address);
  Wire.write(from_addr >> 8);
  Wire.write(from_addr & 0xFF);
  Wire.endTransmission(false);
  Wire.requestFrom(address,sizeof(writePacket));
  while(!Wire.available());
  for(int i=0; i<sizeof(writePacket); i++)
  {
    ((char*)&writePacket)[i] = Wire.read();
  }
  int temperature = (int)myBarometer.bmp085GetTemperature(writePacket.temp);
  unsigned long pressure = myBarometer.bmp085GetPressure(writePacket.pres);
  Serial.println("z");
  Serial.println(writePacket.yr); 
  Serial.println(writePacket.mo);
  Serial.println(writePacket.da);
  Serial.println(writePacket.ho);
  Serial.println(writePacket.mi);
  Serial.println(writePacket.se);
  Serial.println(temperature);
  Serial.println(pressure);
  Serial.println(writePacket.pad1);
  counter2 += sizeof(writePacket);
}
void readSensors(){
  rtc.getTime();
  rtc.getDate();
  writePacket.yr = rtc.getYear();
  writePacket.mo = rtc.getMonth();
  writePacket.da = rtc.getDay();
  writePacket.ho = rtc.getHour();
  writePacket.mi = rtc.getMinute();
  writePacket.se = rtc.getSecond();
  writePacket.temp = myBarometer.bmp085ReadUT();
  writePacket.pres = myBarometer.bmp085ReadUP();
  DHT.read22(DHT22_PIN);
  writePacket.pad1 = (int)DHT.humidity;
  writePacket.pad2 = 0xff;
}


