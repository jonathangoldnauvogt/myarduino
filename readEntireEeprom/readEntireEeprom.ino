#include <Wire.h>


void setup() {
  pinMode(10, OUTPUT);
  digitalWrite(10, HIGH);
  pinMode(11, OUTPUT);
  digitalWrite(11, HIGH);
 
  // put your setup code here, to run once:
Serial.begin(115200);
Wire.begin();
}
int counter = 0;
void loop() {
  // put your main code here, to run repeatedly: 
  Serial.print(" ");
  Serial.print(eeprom_i2c_read(0x50,counter),HEX);
  counter++;
  delay(1);
}



byte eeprom_i2c_read(int address, int from_addr) {
  
  Wire.beginTransmission(address);
  Wire.write(from_addr >> 8);
  Wire.write(from_addr & 0xFF);
  Wire.endTransmission(false);
  Wire.requestFrom(address,1);
  while(!Wire.available());
  return Wire.read();
  
}
