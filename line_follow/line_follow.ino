#define LEFT_MOTOR_ENABLE 13
#define LEFT_MOTOR_A 10
#define LEFT_MOTOR_B 9
#define RIGHT_MOTOR_ENABLE 6
#define RIGHT_MOTOR_A 8
#define RIGHT_MOTOR_B 7
#include <DueTimer.h>


void setup() {
  pinMode(LEFT_MOTOR_ENABLE,OUTPUT);
  pinMode(LEFT_MOTOR_A,OUTPUT);
  pinMode(LEFT_MOTOR_B,OUTPUT);
  pinMode(RIGHT_MOTOR_ENABLE,OUTPUT);
  pinMode(RIGHT_MOTOR_A,OUTPUT);
  pinMode(RIGHT_MOTOR_B,OUTPUT);
  
  Timer3.attachInterrupt(driveTimer);
  Timer3.start(3000);

}
int timeSinceBlack;
int timeSinceWhite;
int light;
int secondWheel;
void loop() {
  
  
  light = analogRead(A0);
  
  
}

void driveTimer(){
   
  if(light <600) {
    secondWheel = 100-(timeSinceWhite*2);
    if (secondWheel<-255)secondWheel = -255;
    drive(255, secondWheel);
    timeSinceBlack = 0;
    timeSinceWhite = timeSinceWhite + 2;
  }
  else {
    secondWheel = 100-timeSinceBlack;
    if (secondWheel<-255)secondWheel = -255;
    drive(secondWheel,255);
    timeSinceWhite = 0;
    timeSinceBlack++;
  }
  
}
void drive(int rightSpeed, int leftSpeed){
  analogWrite(LEFT_MOTOR_B, (leftSpeed<0)?LOW:leftSpeed);
  analogWrite(LEFT_MOTOR_A, (leftSpeed>0)?LOW:abs(leftSpeed));
  digitalWrite(LEFT_MOTOR_ENABLE, HIGH);

  analogWrite(RIGHT_MOTOR_B, (rightSpeed<0)?LOW:rightSpeed);
  analogWrite(RIGHT_MOTOR_A, (rightSpeed>0)?LOW:abs(rightSpeed));
  digitalWrite(RIGHT_MOTOR_ENABLE, HIGH);
}

