  


#include "Tlc5940.h"

#define MINBRIGHT 5

int brightness = 0;
const int xValues[] = {
18, 18, 17, 14, 9, 4, 1, 0, 1, 3, 4, 6, 8, 11, 13, 15, 17, 19, 20, 20, 18, 13, 8, 7, 8, 9, 12, 19,
22, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 26, 35, 43, 47, 49, 50, 48, 44, 42, 41, 46, 49, 59,
59, 59, 58, 58, 58, 58, 59, 59, 59, 59, 59, 59, 65, 70, 73, 72, 69, 65, 78, 80, 81, 82, 82, 82, 82, 
82, 84, 88, 90, 93, 97, 98, 99, 100
};

const int yValues[] = {
  80, 86, 92, 97, 100, 98, 94, 88, 82, 77, 71, 66, 60, 55, 50, 44, 38, 33, 27, 21, 16, 14, 17, 23,
  29, 34, 40, 49, 53, 87, 81, 75, 69, 63, 57, 51, 45, 39, 33, 27, 65, 72, 50, 54, 59, 65, 71, 67,
  61, 55, 45, 40, 71, 65, 60, 54, 48, 42, 36, 30, 24, 18, 12, 6, 0, 70, 71, 66, 61, 56, 51, 97, 91,
  85, 79, 73, 67, 61, 55, 49, 54, 60, 65, 61, 55, 49, 43
};


int ledMapping[] = {
  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33,
  34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52,
  53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71,
  72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87
};





int s[] = {
  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 27, 28, 29
};
int t[] = {
  30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 41, 42
};
int e[] = {
  43, 44, 45, 46, 47, 48, 49, 50, 51, 52
};
int p[] = {
  53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71
};
int h[] = {
  72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87
};
  
  
  
int mode = 0;

void setup()
{
  
  /* Call Tlc.init() to setup the tlc.
     You can optionally pass an initial PWM value (0 - 1000) for all channels.*/
  Tlc.init();
  TCCR0B = TCCR0B & 0b11111000 | 0x01;
  TCCR1B = TCCR1B & 0b11111000 | 0x01;
  TCCR2B = TCCR2B & 0b11111000 | 0x01;
  attachInterrupt(0, modeChange, RISING);
  Serial.begin(9600);
}
 void modeChange(){
 mode++;
}

void loop() {
  brightness = analogRead(A1)*4;
  if(mode == 0) regular();
  if(mode == 1) computer();
 // if(mode == 2) white();
 // if(mode == 3) slowFade();
  //if(mode == 4) alternateFlash(255,4);
 // if(mode == 5) alternateFlash(50,5);
 // if(mode == 6) sineFade();
 // if(mode == 7) sineFade2();
  if(mode > 1) mode = 0;
 
}
byte data;
int nextByte = 0;
int nextLed = 0;
int nextLedBrightness = 1;
//bool latch = false;
void serialEvent(){
  while(Serial.available()){
  
  data = Serial.read();
  
    switch (nextByte){
    case 0:
    
    break;
    case 1:
    nextLed = data;
    
    break;
    case 2:
    nextLedBrightness = 1;
    
    case 3:
    Tlc.set(nextLed, ledBrightness(nextLedBrightness*4095));
    
    Tlc.update();
    break;
    case 4:
    Tlc.clear();
    break;
    
    }
     
    
  switch (data){
    case 0xed:
    nextByte = 1;
    break;
   
    case 0xee:
    nextByte = 2;
    break;
    
    case 0xef:
    nextByte = 3;
    break;
    case 0xf0:
    nextByte = 4;
    break;
    default:
    nextByte = 0;
    
  }
  }

}

//while(!Serial.available()){};
//  int data2 = Serial.read();
  //while(
  //Tlc.set(data2,ledBrightness());
  ///Tlc.update(); 
  //delay(500);
        
 // }




void computer(){
  /*
  byte data = 0x50;
  Tlc.clear();
  while(Serial.available()){
    data = Serial.read();
    Serial.println(data);
    if (data == 0x0f){
      while(!Serial.available()){};
        int data2 = Serial.read();
        Tlc.set(data2,ledBrightness(4095));
        Tlc.update(); 
        delay(500);
        
    }
    
  }
  
  */
  
}
void regular()
{
  
  for(int j=0; j<1;j++){
  
  for(int i = 2; i < 4095; i=i+i^2){
  
     clearWithBackLight(4095, MINBRIGHT);
    for(int steph = 0; steph<87; steph +=2){
    
  for(int s = 0; s<88;s++){
  
    Tlc.set(steph, ledBrightness(i));
  }
 
 
    }
    Tlc.update();
    delay(10);

  }



for(int i = 2; i < 100; i=i+i^2){
 clearWithBackLight(4095, MINBRIGHT);
  for(int steph = 1; steph<87; steph +=2){
    
  
  
  
    Tlc.set(steph, ledBrightness(i));
 
 
 
 
  }
Tlc.update();
delay(10);

}
  }


  
  for(int i = 0; i < 100; i= i+i^2){
  
    clearWithBackLight(4095,MINBRIGHT);
    for(int i2 = 0; i2< 4095/4; i2+=100){
      for(int i = 0; i< 87; i++){
        Tlc.set(i, ledBrightness(i2));
        Tlc.update();
      }
      delay(i);
     }
  }
clearWithBackLight(4095,MINBRIGHT);
  for(int i = 2; i<4000;i=i+i^2){
  delay(100);
  displayLetterS(i);
  delay(100);
  clearWithBackLight(4095,MINBRIGHT);
  displayLetterT(i);
  delay(100);
  clearWithBackLight(4095,MINBRIGHT);
  displayLetterE(i);
  delay(100);
  clearWithBackLight(4095,MINBRIGHT);
  displayLetterP(i);
  delay(100);
  clearWithBackLight(4095,MINBRIGHT);
  displayLetterH(i);  
  delay(100);
  clearWithBackLight(4095,MINBRIGHT);
  
  }
  
  
  clearWithBackLight(4095,MINBRIGHT);
  for(int i = 2; i<4000;i=i+i^2){
  delay(100);
  displayLetterS(i);
  delay(100);

  displayLetterT(i);
  delay(100);

  displayLetterE(i);
  delay(100);

  displayLetterP(i);
  delay(100);
 
  displayLetterH(i);  
  delay(100);

    clearWithBackLight(4095,MINBRIGHT);
  }

  
  delay(1000);
  
  

  

  
  
  //sweep demo
  clearWithBackLight(4095,MINBRIGHT);
  for(int xBound = 0; xBound <= 100; xBound+=1){
    for(int i = 0; i< 87; i++){
      if(xValues[i]<=xBound){
        Tlc.set(i,1000);
      }
      Tlc.update();
    
    }
    delay(20);
  }
  delay(1000);
  clearWithBackLight(4095,MINBRIGHT);
  for(int yBound = 0; yBound <= 100; yBound+=1){
    for(int i = 0; i< 87; i++){
      if(yValues[i]<=yBound){
        Tlc.set(i,1000);
      }
      Tlc.update();
    
    }
    delay(20);
  }
  delay(1000);
  clearWithBackLight(4095,MINBRIGHT);
    for(int yBound = 0; yBound <= 100; yBound+=1){
    for(int i = 0; i< 87; i++){
      if((yValues[i] + xValues[i]) <=2*yBound){
        Tlc.set(i,ledBrightness(4095));
      }
      Tlc.update();
    
    }
    delay(20);
  }
  
  
  
  //gradient demo
  
  delay(1000);
    clearWithBackLight(4095,MINBRIGHT);
  for(int xBound = 0; xBound <= 100; xBound+=1){
    for(int i = 0; i< 87; i++){
      if(xValues[i]<=xBound){
        Tlc.set(i,ledBrightness(random(4095)));
      }
      Tlc.update();
    
    }
    delay(20);
  }
  clearWithBackLight(4095,MINBRIGHT);
  for(int xBound = 0; xBound <= 100; xBound+=1){
    for(int i = 0; i< 87; i++){
      if(yValues[i]<=xBound){
        Tlc.set(i,ledBrightness(random(4095)));
      }
      Tlc.update();
    
    }
    delay(20);
  }
  delay(1000);
  
  clearWithBackLight(4095,MINBRIGHT);
    for(int xBound = 0; xBound <= 200; xBound+=1){
    for(int i = 0; i< 87; i++){
      if((yValues[i] + xValues[i])<=2*xBound){
        Tlc.set(i,ledBrightness(random(4095)));
      }
      Tlc.update();
    
    }
    delay(20);
  }
  
  
}
  
  /*
  int direction = 1;
  for (int channel = 0; channel < NUM_TLCS * 16; channel += direction) {

    /* Tlc.clear() sets all the grayscale values to zero, but does not send
       them to the TLCs.  To actually send the data, call Tlc.update() 
    clearWithBackLight(4095);

    /* Tlc.set(channel (0-15), value (0-1000)) sets the grayscale value for
       one channel (15 is OUT15 on the first TLC, if multiple TLCs are daisy-
       chained, then channel = 16 would be OUT0 of the second TLC, etc.).

       value goes from off (0) to always on (1000).

       Like Tlc.clear(), this function only sets up the data, Tlc.update()
       will send the data. 
    if (channel == 0) {
      direction = 1;
    } else {
      Tlc.set(channel - 1, 1000);
    }
    Tlc.set(channel, 4095);
    if (channel != NUM_TLCS * 16 - 1) {
      Tlc.set(channel + 1, 1000);
    } else {
      direction = -1;
    }

    /* Tlc.update() sends the data to the TLCs.  This is when the LEDs will
       actually change. 
    Tlc.update();

    delay(75);
  }

 */
 
 void displayLetterE(int d){
  for(int i = 0; i< sizeof(e)/sizeof(int); i++){
    Tlc.set(e[i]-1,ledBrightness(4095));
    delay(d);
    Tlc.update();
  }
  Tlc.update();
}
void displayLetterS(int d){
  for(int i = 0; i< sizeof(s)/sizeof(int); i++){
    Tlc.set(s[i]-1,ledBrightness(4095));
  delay(d);
    Tlc.update();  
  }
 
}
void displayLetterT(int d){

  for(int i = 0; i< sizeof(t)/sizeof(int); i++){
    Tlc.set(t[i]-1,ledBrightness(4095)); 
     delay(d);
    Tlc.update();   
  }

}
void displayLetterP(int d){
  for(int i = 0; i< sizeof(p)/sizeof(int); i++){
    Tlc.set(p[i]-1,ledBrightness(4095));
    delay(d);
    Tlc.update();    
  }

}
void displayLetterH(int d){
  for(int i = 0; i< sizeof(h)/sizeof(int); i++){
    Tlc.set(h[i]-1,ledBrightness(4095)); 
     delay(d);
    Tlc.update();   
  }

}
  
int ledBrightness(int value){
 
  //return value;
  
  //pow(value, 2);
  value = value*((float)brightness/4095)/2;
  //(1/(1+pow(2.7,((value/21)-6)*-1))*1000);
  if(value>brightness)value = brightness;
  return value;
}
void clearWithBackLight(int backIntensity, int others){
    Tlc.clear();
 
    for(int i = 87; i<= 98; i++){
      Tlc.set(i, ledBrightness(backIntensity/2));
      
    }
    for(int i = 0; i< 87; i++){
      Tlc.set(i, ledBrightness(others));
      
    }
  Tlc.update();
  
  
}


