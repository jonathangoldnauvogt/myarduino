/* UART Example, any character received on either the real
   serial port, or USB serial (or emulated serial to the
   Arduino Serial Monitor when using non-serial USB types)
   is printed as a message to both ports.

   This example code is in the public domain.
*/

// This line defines a "Uart" object to access the serial port

char counter = 0;
char lemon[6];
void setup() {
	Serial1.begin(9600);
        Serial2.begin(9600);
        pinMode(13, OUTPUT);
}

void loop() {
        char incomingByte;
        
	
	if (Serial1.available() > 0) {
  
		incomingByte = Serial1.read();
                if (incomingByte == 0x0D){ 
                }
                else{
		lemon[counter] = incomingByte;
		Serial2.write(incomingByte);
                counter = ++counter % 6;
                }
                if (counter == 5){
                  for(int i; i < 6;i++){
                   Serial.print(lemon[i], DEC);
                   Serial.print(" ");
                  }
                  Serial.println();
                }
                if (!counter)
                  analogWrite(13,lemon[0]);
	}
}

