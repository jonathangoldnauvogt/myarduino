/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
int greenL = 6;
int blueL = 5;
int redL = 3;
int blueY = 9;
int greenY = 10;
int redY = 11;

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(greenL, OUTPUT); 
  pinMode(blueL, OUTPUT);
  pinMode(redL, OUTPUT);
  pinMode(blueY, OUTPUT);
  pinMode(greenY, OUTPUT);
  pinMode(redY, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  digitalWrite(greenL, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);               // wait for a second
  //digitalWrite(greenL, LOW);    // turn the LED off by making the voltage LOW
  delay(100);
  
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);               // wait for a second
  //digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(100); 
  
  digitalWrite(led2, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);               // wait for a second
  //digitalWrite(led2, LOW);    // turn the LED off by making the voltage LOW
  delay(100); 
  
  digitalWrite(led3, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);               // wait for a second
  //digitalWrite(led3, LOW);    // turn the LED off by making the voltage LOW
  delay(100);   // wait for a second
  
  digitalWrite(led4, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);               // wait for a second
  //digitalWrite(led4, LOW);    // turn the LED off by making the voltage LOW
  delay(100);   // wait for a second
  
  digitalWrite(led5, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);               // wait for a second
  //digitalWrite(led5, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);   // wait for a second
}
