const float pi = 3.14;
float x = 0;
int sineGreen;
int sineRed;
int sineBlue;

float currentTemp = 0;
long green, red, blue;
int sensorPin;
int greenL = 6;
int blueL = 5;
int redL = 3;
int blueY = 9;
int greenY = 10;
int redY = 11;
int mode = 0;

void setup() {
  
  TCCR0B = TCCR0B & 0b11111000 | 0x01;
  TCCR1B = TCCR1B & 0b11111000 | 0x01;
  TCCR2B = TCCR2B & 0b11111000 | 0x01;
  //Serial.begin(9600);
  pinMode(greenL, OUTPUT); 
  pinMode(blueL, OUTPUT);
  pinMode(redL, OUTPUT);
  pinMode(blueY, OUTPUT);
  pinMode(greenY, OUTPUT);
  pinMode(redY, OUTPUT);
  analogReference(INTERNAL);
  delay(5);
  
  for (int i = 0; i<10; i++){
    currentTemp = currentTemp + (analogRead(sensorPin) / 9.31);
  }
   currentTemp = currentTemp/10;
   //Serial.println("Temperature is:");
   //Serial.println(currentTemp);
   
   attachInterrupt(0, modeChange, RISING);
}




void loop() {
  if(mode == 0) tempColour();
  if(mode == 1) blinky1();
  if(mode == 2) white();
  if(mode == 3) slowFade();
  if(mode == 4) alternateFlash(255,4);
  if(mode == 5) alternateFlash(50,5);
  if(mode == 6) sineFade();
  if(mode == 7) sineFade2();
  if(mode > 7) mode = 0;
 
}
void alternateFlash(int intensity, int currentMode){
  colourize(intensity,0,0,0,0,intensity);
  if(mode != currentMode)return;
  delay(5);
  colourize(0,intensity,0,0,intensity,0);
  if(mode != currentMode)return;
  delay(5);
  colourize(0,0,intensity,intensity,0,0);
  if(mode != currentMode)return;
  delay(5);

}

void sineFade2(){
  if(x > 2*pi)x =0;
  sineGreen = sin(x+(((float)pi)/3))*(float)255;
  sineRed =sin(x)*(float)255;
  sineBlue =sin(x+((float)2*(float)pi)/((float)3))*(float)255;
  if(sineGreen < 0)sineGreen = 0;
  if(sineRed < 0)sineRed = 0;
  if(sineBlue < 0)sineBlue = 0;
  colourize(sineRed, sineGreen, sineBlue, sineRed, sineGreen, sineBlue);
  x=x+(pi/32);
  delay(5);
}
void sineFade(){
  if(x > 2*pi)x =0;
  sineGreen =sin(x+(((float)2*(float)pi)/3))*(float)255;
  sineRed =sin(x)*(float)255;
  sineBlue =sin(x+((float)4*(float)pi)/((float)3))*(float)255;
  if(sineGreen < 0)sineGreen = 0;
  if(sineRed < 0)sineRed = 0;
  if(sineBlue < 0)sineBlue = 0;
  colourize(sineRed, sineGreen, sineBlue, sineRed, sineGreen, sineBlue);
  x=x+(pi/32);
  delay(5);
}
void slowFade(){
  for(int i = 0; i <256; i++){
    if(mode != 3)return;
    colourize(i,i,i,i,i,i);
  delay(1);
  }
  for(int i = 25; i >=0; i--){
    if(mode != 3)return;
    colourize(i,i,i,i,i,i);
  delay(1);
  }
  
}

int ledBrightness(int value){
  //return value;
  return (1/(1+pow(2.7,((value/21)-6)*-1))*255);
}


void tempColour(){
  delay(50); 
  float temp = 0;
   for (int i = 0; i<10; i++){
    temp = temp + ((float)analogRead(sensorPin) / 9.31);
  }
   temp = temp/10;
  float deviation = temp - currentTemp;
  deviation = deviation*(float)5;
  
  
  if(deviation > 0){
    green = ceil((-deviation) *150)+255;
  }
  else{
   green = ceil(((deviation) *(float)150)+255);
  }
  
  //green = (pow(deviation*2,4)*-10)+255;
  
  red =floor( -deviation *(float)150.00);
  blue =floor( deviation *(float)150.00);
  //green = (pow(deviation*2,4)*-10)+255;
  //red = (pow(10,-deviation)*200)-150;
  //blue = (pow(10,deviation)*200)-150;
  //Serial.println(blue);
  if (green < 0) green = 0;
  if (green > 255) green = 255;
  if (red < 0) red = 0;
  if (red > 255) red = 255;
  if (blue < 0) blue = 0;
  if (blue > 255) blue = 255;
  
  
  //Serial.println("deviation");
  //Serial.println(deviation);
  colourize(red,green,blue,red,green,blue);
}
 void modeChange(){
 mode++;
}
  
void blinky1(){
 for(int i = 1000; i>1; i--){
   if(mode != 1) return;
  colourize(255,0,0,255,0,0);
  delay(i);
  colourize(0,255,0,0,255,0);
    delay(i);
  colourize(0,0,255,0,0,255);
    delay(i);
 }
}
void white(){
colourize(255,255,255,255,255,255);
delay(2);
}
void colourize(int lR, int lG, int lB, int yR, int yG, int yB){
  analogWrite(greenL,ledBrightness(lG));
  analogWrite(greenY,ledBrightness(yG));
  analogWrite(blueL,ledBrightness(lB));
  analogWrite(blueY,ledBrightness(yB));
  analogWrite(redL,ledBrightness(lR));
  analogWrite(redY,ledBrightness(yR));
}
  
