//MultiWii PPM channel time range is 561 us to 1562 us
#define PPM_START 500
#define PPM_END 2000
double ppm_multiply;
IntervalTimer myTimer;
volatile int output[6];
const int ledPin = LED_BUILTIN;
int ppmState = LOW;
char chanIndex = 0;
int period = 0;
int outputValue;
char counter = 0;
char lemon[6];
void setup(void) {
  
  Serial1.begin(9600);
  Serial2.begin(9600);
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600);
  
  //calculate PPM conversion elements
  ppm_multiply = (PPM_END - PPM_START)/255;
  
  myTimer.begin(blinkLED, 2000000);  // blinkLED to run every 0.15 seconds
  //output[0] = 0;
  //output[1] = 0;
  //output[2] = 0;
  //output[3] = 0;
  //output[4] = 0;
  //output[5] = 0;
}

void blinkLED(void) {
  if(ppmState == HIGH){
    ppmState = LOW;
    outputValue = 500;
  }
  else{
    ppmState = HIGH;
    if (chanIndex >=6){
      outputValue = 20000- period;
      period = 0;
      chanIndex = 0;
    }
    else{
      outputValue = (output[chanIndex]*ppm_multiply)+PPM_START;
      chanIndex++;
    } 
  }
  digitalWrite(ledPin, ppmState);
  myTimer.end();
  myTimer.begin(blinkLED, outputValue);
  if (chanIndex != 0)period = period + outputValue;
}

int number;
void loop(void) {


number = 170;
  for (int i; i<6;i++){
 
    output[i] = (lemon[i]-65)*3;//random(0,255);
  }
   
      
      char incomingByte;
      
      	
    if (Serial1.available() > 0) {
        
      incomingByte = Serial1.read();
      if (incomingByte == 0x0D){ 
      }
      else{
        lemon[counter] = incomingByte;
        Serial2.write(incomingByte);
        counter = ++counter % 6;
      }
      if (counter == 5){
      for(int i; i < 6;i++){
        Serial.print(lemon[i], DEC);
        Serial.print(" ");
      }
      Serial.println();
      }
    }
  }
